/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef  TAUANALYSISTOOLS_TAUTRUTHMATCHINGTOOL_H
#define  TAUANALYSISTOOLS_TAUTRUTHMATCHINGTOOL_H

/*
  author: Dirk Duschinger
  mail: dirk.duschinger@cern.ch
  documentation in: ../README.rst
*/

// Local include(s):
#include "TauAnalysisTools/ITauTruthMatchingTool.h"
#include "TauAnalysisTools/BuildTruthTaus.h"
#include "CxxUtils/CachedValue.h"

namespace TauAnalysisTools
{

class TauTruthMatchingTool
  : public virtual TauAnalysisTools::BuildTruthTaus
  , public virtual TauAnalysisTools::ITauTruthMatchingTool
{
  /// Create a proper constructor for Athena
  ASG_TOOL_CLASS2( TauTruthMatchingTool,
                   TauAnalysisTools::IBuildTruthTaus,
                   TauAnalysisTools::ITauTruthMatchingTool )

public:                         // Interface functions

  TauTruthMatchingTool( const std::string& name );

  virtual ~TauTruthMatchingTool() = default;

  // initialize the tool
  virtual StatusCode initialize() override final;

  virtual std::unique_ptr<ITruthTausEvent> getEvent() const override final;

  // get pointer to the truth matched particle, if no truth particle was found a null pointer is returned
  virtual const xAOD::TruthParticle* getTruth(const xAOD::TauJet& xTau) override final;
  virtual const xAOD::TruthParticle* getTruth(const xAOD::TauJet& xTau,
                                              ITruthTausEvent& truthTausEVent) const override final;
  virtual std::vector<const xAOD::TruthParticle*> getTruth(const std::vector<const xAOD::TauJet*>& vTaus) override final;

public:                         // Wrapper functions
  
  // wrapper function to get truth tau visible TLorentzvector
  virtual TLorentzVector getTruthTauP4Vis(const xAOD::TauJet& xTau) override final;
  virtual TLorentzVector getTruthTauP4Vis(const xAOD::TruthParticle& xTruthTau) const override final;

  // wrapper function to get truth tau invisible TLorentzvector
  virtual TLorentzVector getTruthTauP4Invis(const xAOD::TauJet& xTau) override final;
  virtual TLorentzVector getTruthTauP4Invis(const xAOD::TruthParticle& xTruthTau) const override final;

  // get type of truth match particle (hadronic tau, leptonic tau, electron, muon, jet)
  virtual TauAnalysisTools::TruthMatchedParticleType getTruthParticleType(const xAOD::TauJet& xTau) override final;

  // wrapper function to count number of decay particles of given pdg id
  virtual int getNTauDecayParticles(const xAOD::TauJet& xTau, int iPdgId, bool bCompareAbsoluteValues = false) override final;
  virtual int getNTauDecayParticles(const xAOD::TruthParticle& xTruthTau, int iPdgId, bool bCompareAbsoluteValues = false) const override final;

  // wrapper function to obtain truth verion of xAOD::TauJetParameters::DecayMode
  virtual xAOD::TauJetParameters::DecayMode getDecayMode(const xAOD::TauJet& xTau) override final;
  virtual xAOD::TauJetParameters::DecayMode getDecayMode(const xAOD::TruthParticle& xTruthTau) const override final;

  /// Decorations produced by an algorithm need to be locked; otherwise, they
  /// will be ignored by deep copies.  The interfaces of this tool
  /// take a single object at a time, so we can't do the locking there.
  /// Instead, call this method after all taus in a container have been
  /// processed.
  virtual StatusCode lockDecorations (const xAOD::TauJetContainer& taus) const override final;

private:                        // private helper functions

  StatusCode findTruthTau(const xAOD::TauJet& xTau,
                          TruthTausEvent& truthTausEvent) const;
  StatusCode checkTruthMatch (const xAOD::TauJet& xTau, const xAOD::TruthParticleContainer& xTauContainer, const TruthTausEvent& truthTausEvent) const;

private:                        // steering variables

  double m_dMaxDeltaR;

  CxxUtils::CachedValue<bool> m_bIsTruthMatchedAvailable;
  CxxUtils::CachedValue<bool> m_bIsTruthParticleLinkAvailable;

private:                        // private helper variables

  SG::ConstAccessor<double> m_accPtVis;
  SG::ConstAccessor<double> m_accEtaVis;
  SG::ConstAccessor<double> m_accPhiVis;
  SG::ConstAccessor<double> m_accMVis;

}; // class TauTruthMatchingTool

}
#endif // TAUANALYSISTOOLS_TAUTRUTHMATCHINGTOOL_H
