/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/* *******************************************************************

   TRTCalibrationMgr.cxx : Manager for TRT calibration

   * ***************************************************************** */

#include "TRT_CalibAlgs/TRTCalibrationMgr.h"

#include "TrkTrack/Track.h"

#include "TRT_CalibData/TrackInfo.h"
#include "TRT_ConditionsData/FloatArrayStore.h"
#include "TRT_CalibTools/IFillAlignTrkInfo.h"
#include "TRT_CalibTools/IFitTool.h"
#include "TrkFitterInterfaces/ITrackFitter.h"
#include "TrkToolInterfaces/ITrackSelectorTool.h"
#include "TrkTrack/TrackCollection.h"
#include "TrkTrack/Track.h"
#include "VxVertex/VxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "TROOT.h"
#include "StoreGate/ReadHandle.h"

TRTCalibrationMgr::TRTCalibrationMgr(const std::string& name, ISvcLocator* pSvcLocator) :
	AthAlgorithm   (name, pSvcLocator){}

//---------------------------------------------------------------------

TRTCalibrationMgr::~TRTCalibrationMgr(void)
{}

//--------------------------------------------------------------------------

StatusCode TRTCalibrationMgr::initialize(){
	ATH_MSG_DEBUG("initialize()");

	if (m_docalibrate){
		if (!m_TRTCalibTools.size() || m_TRTCalibTools.retrieve().isFailure())
		{
			ATH_MSG_FATAL("Cannot get Calibration tool " << m_TRTCalibTools);
			return StatusCode::FAILURE;
		}
	}
	else{
		if (!m_TrackInfoTools.size() || m_TrackInfoTools.retrieve().isFailure())
		{
			ATH_MSG_FATAL("Cannot get TrackInfo filler tool " << m_TrackInfoTools);
			return StatusCode::FAILURE;
		}
	}

	if (!m_FitTools.size() || m_FitTools.retrieve().isFailure()){
		ATH_MSG_FATAL("Cannot get Fit tools " << m_FitTools);
		return StatusCode::FAILURE;
	}

	if (m_trackFitter.retrieve().isFailure()){
		ATH_MSG_FATAL("Failed to retrieve tool " << m_trackFitter);
		return StatusCode::FAILURE;
	}

	if (m_writeConstants){
		ATH_CHECK(m_streamer.retrieve());
	}
		

	// Initialize ReadHandles and ReadHandleKeys
	ATH_CHECK(m_verticesKey.initialize());
	ATH_CHECK(m_EventInfoKey.initialize());
	ATH_CHECK(m_TrkCollection.initialize());

	ATH_MSG_INFO("Tracks from Trk::Track collection:");
	ATH_MSG_INFO("\n\t" << m_TrkCollection);

	// Get the Track Selector Tool
	if (!m_trackSelector.empty())
	{
		StatusCode sc = m_trackSelector.retrieve();
		if (sc.isFailure())
		{
			ATH_MSG_FATAL("Could not retrieve " << m_trackSelector << " (to select the tracks which are written to the ntuple) ");
			ATH_MSG_INFO("Set the ToolHandle to None if track selection is supposed to be disabled");
			return sc;
		}
	}

	ATH_MSG_INFO("Track Selector retrieved");

	m_ntrk = 0;

	return StatusCode::SUCCESS;
}

//---------------------------------------------------------------------

StatusCode TRTCalibrationMgr::execute(){

    if (m_docalibrate){
        ATH_MSG_INFO("skipping execute() calibrating instead");
        bool CalibOK = m_TRTCalibTools[0]->calibrate();
        if (CalibOK)
        {
            return StatusCode::SUCCESS;
        }
        else
        {
            return StatusCode::FAILURE;
        }
    }

    if (m_writeConstants)
    {
        StatusCode sc = streamOutCalibObjects();
        return sc;
    }

    // Get Primary vertices. Skip events without three good tracks on vertex.

    SG::ReadHandle<xAOD::VertexContainer> vertices(m_verticesKey);
    if (not vertices.isValid())
    {
        ATH_MSG_DEBUG("Couldn't retrieve VertexContainer with key: PrimaryVertices");
        return StatusCode::SUCCESS; // just skip to next event in case of no vertexcontainer
    }

    int countVertices(0);
    for (const xAOD::Vertex *vx : *(vertices.cptr()))
    {
        if (vx->vertexType() == xAOD::VxType::PriVtx)
        {
            if (vx->nTrackParticles() >= 3)
                countVertices++;
        }
    }
    if (countVertices < 1)
    {
        ATH_MSG_INFO("no vertices found");
        return StatusCode::SUCCESS;
    }

    // get event info pointer
    SG::ReadHandle<xAOD::EventInfo> EventInfo(m_EventInfoKey);
    if (not EventInfo.isValid())
    {
        ATH_MSG_FATAL("skipping event, could not get EventInfo");
        return StatusCode::FAILURE;
    }

    // Loop over tracks; get track info and accumulate it
    const Trk::Track *aTrack;

    SG::ReadHandle<TrackCollection> trks(m_TrkCollection);
    // retrieve all tracks, but only from one collection (CombinedInDetTracks)

    if (trks.isValid())
    {

        if (trks->size() < 3)
        {
            ATH_MSG_INFO("skipping event, it contains only " << trks->size() << " tracks (less than 3)");
            return StatusCode::SUCCESS;
        }

        if (trks->size() > m_max_ntrk)
        {
            ATH_MSG_INFO("skipping event, it contains " << trks->size() << " tracks, more than max: " << m_max_ntrk);
            return StatusCode::SUCCESS;
        }
        for (TrackCollection::const_iterator t = trks->begin(); t != trks->end(); ++t)
        {

            if (m_trackSelector->decision(*(*t), nullptr))
            {

                m_ntrk++;
                aTrack = *t;

                if (m_dorefit)
                {
                    // Refit Track with new ROT creator
                    Trk::RunOutlierRemoval runOutlier = true;
                    aTrack = m_trackFitter->fit(Gaudi::Hive::currentContext(), *aTrack, runOutlier, aTrack->info().particleHypothesis()).release();
                }
                // Check selection if requested
                if (aTrack)
                {
                    // fill track info
                    TRT::TrackInfo at;
                    // Run, event, track id
                    at[TRT::Track::run] = EventInfo->runNumber();
                    at[TRT::Track::event] = EventInfo->eventNumber();
                    at[TRT::Track::trackNumber] = m_ntrk;
                    ATH_MSG_DEBUG("  Track " << m_ntrk << " accepted Info: run="
                                             << at[TRT::Track::run] << "   event=" << at[TRT::Track::event]);
                    for (unsigned int j = 0; j < m_TrackInfoTools.size(); j++)

                        if (!m_TrackInfoTools[j]->fill(aTrack, &at, *EventInfo, *vertices))
                            break;
                    if (m_dorefit)
                        delete aTrack;
                }
            }
        }
    }
    return StatusCode::SUCCESS;
}

//---------------------------------------------------------------------

StatusCode TRTCalibrationMgr::finalize(){
  ATH_MSG_INFO( "CALIBSTAT CM_TRKS: " << m_ntrk);

	gROOT->SetMustClean(false);

	// Accumulators to finalize
	std::vector<IdentifierProfileHistogram*> histograms;
	for (unsigned int j=0;j<m_TrackInfoTools.size();j++)
		if ((m_TrackInfoTools[j]->finalize()).isFailure()){
		  ATH_MSG_FATAL( "Error calling TrackInfo tool finalize ");
			return StatusCode::FAILURE;
		}

	return StatusCode::SUCCESS;
}

StatusCode TRTCalibrationMgr::streamOutCalibObjects()
{
  ATH_MSG_INFO( "entering streamOutCalibObjects " );
  StatusCode sc = m_streamer->connectOutput();
  if (sc.isFailure()) {
    ATH_MSG_ERROR("Could not connect stream to output");
    return( StatusCode::FAILURE);
  }
  
  IAthenaOutputStreamTool::TypeKeyPairs typeKeys;
  typeKeys.push_back( IAthenaOutputStreamTool::TypeKeyPair(StrawT0Container::classname(),m_par_t0containerkey)) ;
  typeKeys.push_back( IAthenaOutputStreamTool::TypeKeyPair(RtRelationContainer::classname(),m_par_rtcontainerkey)) ;
  
  sc = m_streamer->streamObjects(typeKeys);
  if (sc.isFailure()) {
    ATH_MSG_ERROR("Could not stream out Containers ");
    return( StatusCode::FAILURE);
  }
  
  sc = m_streamer->commitOutput();
  if (sc.isFailure()) {
    ATH_MSG_ERROR("Could not commit output stream");
    return( StatusCode::FAILURE);
  }
  
  ATH_MSG_INFO( "   Streamed out and committed "  << typeKeys.size() << " objects " );
  return StatusCode::SUCCESS;
}

