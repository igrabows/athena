#include "../LArFEBTempAlg.h"
#include "../LArHVPathologyDbAlg.h"
#include "../LArHV2Ntuple.h"
#include "../LArHVCorrToSCHVCorr.h"
#include "../LArHVlineMapAlg.h"



DECLARE_COMPONENT( LArFEBTempAlg )
DECLARE_COMPONENT( LArHVPathologyDbAlg )
DECLARE_COMPONENT( LArHV2Ntuple )
DECLARE_COMPONENT( LArHVCorrToSCHVCorr )
DECLARE_COMPONENT( LArHVlineMapAlg )

