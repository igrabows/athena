#!/bin/bash
#
# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Script for building the Athena project.
#

# Set up the variables necessary for the script doing the heavy lifting.
ATLAS_PROJECT_NAME="Athena"
ATLAS_PROJECT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
ATLAS_BUILDTYPE="Release"
ATLAS_EXTRA_CMAKE_ARGS=(-DBoost_NO_WARN_NEW_VERSIONS=TRUE
                        -DATLAS_COMPRESS_DEBUG_SECTIONS=TRUE)
ATLAS_EXTRA_MAKE_ARGS=()

# Let "the common script" do all the heavy lifting.
source "${ATLAS_PROJECT_DIR}/../../Build/AtlasBuildScripts/build_project.sh"
