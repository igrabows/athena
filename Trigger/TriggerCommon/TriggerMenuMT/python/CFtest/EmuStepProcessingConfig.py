# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

from AthenaCommon.Logging import logging
from AthenaConfiguration.ComponentAccumulator import CompFactory
import functools

log = logging.getLogger('EmuStepProcessingConfig')

###########################################################################    
def generateEmuMenu(flags):
    """ 
     set Emu menu and reproduce generateMT
    """
    log.info("generateEmuMenu")  
    from TriggerMenuMT.HLT.Menu import Dev_pp_run3_v1
    from TriggerMenuMT.HLT.Menu import Dev_pp_run3_emu_v1 
    from TriggerMenuMT.HLT.Config.GenerateMenuMT import GenerateMenuMT

    # overwrite Dev_pp_run3_v1
    Dev_pp_run3_v1.setupMenu = Dev_pp_run3_emu_v1.setupMenu

    # Generate the menu
    menu = GenerateMenuMT()
    chains = menu.generateAllChainConfigs(flags)
    return chains


###########################################################################    

def generateEmuEvents():
    log.info("generateEmuEvents")
    # 4 events
    data = {
        'noreco': [';', ';', ';',';'],
        'emclusters': [';', ';', ';',';'],
        'msmu': [';', ';', ';',';'],
        'ctp': [';', ';', ';',';'],
        'l1emroi': [';', ';', ';',';'],
        'l1muroi': [';', ';', ';',';']
        }  # in the lists there are the events

    # event 0: empty
    data['ctp'] [0]      =  'HLT_TestChain5_ev1_L1EM3 \
                             HLT_TestChain8_ev1_L1EM3 \
                             HLT_g5_EM7'
    data['l1emroi'][0]   = ';'
    data['emclusters'][0]= ';'
    data['l1muroi'][0]   = ';'
    data['msmu'][0]      = ';'


    #event 1: 3e (1 not passing at L1, 1 not passing at step1) + 2mu (2 not passing) - HLT_e5_e8_L12EM3 HLT_2TestChain6_mv1_L12MU5VF
    data['ctp'] [1]      =  'HLT_TestChain5_ev1_L1EM3 \
                             HLT_TestChain8_ev1_L1EM3 \
                             HLT_TestChain5_gv1_L1EM7 \
                             HLT_TestChain5_ev3_L1EM7 \
                             HLT_2TestChain6_mv1_L12MU5VF \
                             HLT_TestChain10_mv2_L1MU8F \
                             HLT_TestChain6_mv1_TestChain10_mv1_L12MU5VF \
                             HLT_TestChain6_mEmpty3_TestChain10_mv1_L12MU5VF \
                             HLT_2TestChain4_mv1_dr_L12MU5VF'
    data['l1emroi'][1]   =  '1,1,0,EM3,EM7,EM20,EM30,EM100; 2.,-1.2,0,EM3,EM7; 3.,0.2,0,EM3;'
    data['emclusters'][1]=  'eta:1,phi:1,et:180000; eta:2,phi:-1.2,et:6000; eta:3.,phi:0.2,et:3000;'
    data['l1muroi'][1]   =  '2,0.5,0,MU5VF; 3,0.5,0,MU5VF;'
    data['msmu'][1]      = 'eta:2,phi:0.5,pt:1500,pt2:1500; eta:3,phi:0.5,pt:1500,pt2:1500;'

    # event 2: 2e+ 3mu : HLT_TestChain5_ev1_TestChain8_ev1_2TestChain6_mv1_L1_2EM8VH_MU8F, HLT_TestChain6_mv1_TestChain10_ev1_L12eEM10L_MU8F
    data['ctp'] [2]      =  'HLT_TestChain6_mv1_L1MU5VF \
                             HLT_TestChain8_mv1_L1MU8F \
                             HLT_TestChain10_mv2_L1MU8F \
                             HLT_TestChain8_mv1step_L1MU8F \
                             HLT_TestChain5_ev1_L1EM3 \
                             HLT_TestChain8_ev1_L1EM3 \
                             HLT_TestChain8_mEmpty2_L1MU8F \
                             HLT_TestChain8_mEmpty3_L1MU8F \
                             HLT_TestChain6_mEmpty3_TestChain10_mv1_L12MU5VF \
                             HLT_TestChain6_mv1_TestChain10_ev1_L12eEM10L_MU8F \
                             HLT_TestChain6_mv2_TestChain8_ev2_L12eEM10L_MU8F \
                             HLT_2TestChain6_mv1_L12MU5VF \
                             HLT_2TestChain6_mEmpty1_L12MU5VF \
                             HLT_TestChain6_mv1_TestChain10_mv1_L12MU5VF \
                             HLT_2TestChain4_mv1_dr_L12MU5VF HLT_e5_e8_L12EM3 \
                             HLT_TestChain5_ev1_TestChain8_ev1_2TestChain6_mv1_L12EM8VH_MU8F \
                             HLT_TestChain10_mEmpty1_TestChain6_mEmpty1_L12MU5VF \
                             HLT_TestChain10_mv1_TestChain6_mEmpty1_L1MU5VF \
                             HLT_TestChain5_ev1_TestChain8_ev1_merge_L12EM3 \
                             HLT_TestChain5_ev1_TestChain8_ev1_L12EM3  \
                             HLT_TestChain6_mv1_TestChain5_ev1_dr_L12MU5VF'
    data['l1emroi'][2]   =  '0.5,0.1,0,EM3,EM7,EM15,EM20,EM30,EM100; 1,-1.2,0,EM3,EM7,EM15,EM20,EM30;'
    data['emclusters'][2]=  'eta:0.5,phi:0.1,et:120000; eta:1,phi:-1.2,et:65000;'
    data['l1muroi'][2]   =  '-1.2,0.7,0,MU5VF,MU8VF; -1.1,0.6,0,MU5VF,MU8F,MU8VF;1.1,0.6,0,MU5VF;'
    data['msmu'][2]      =  'eta:-1.2,phi:0.7,pt:6500,pt2:8500; eta:-1.1,phi:0.6,pt:10500,pt2:8500;eta:1.1,phi:0.6,pt:8500,pt2:8500;'

    #event 3: 1e + 1mu; HLT_TestChain6_mv1_TestChain10_ev1_L12eEM10L_MU8F does not pass because of e10
    data['ctp'] [3]      =  'HLT_TestChain20_mv1_L1MU8F \
                             HLT_TestChain10_mv1_L1MU8F \
                             HLT_TestChain8_mv1_L1MU8F \
                             HLT_TestChain8_mEmpty3_L1MU8F \
                             HLT_TestChain8_mEmpty2_L1MU8F \
                             HLT_TestChain6_mEmpty3_TestChain8_mv1_L12MU5VF \
                             HLT_TestChain8_mv1step_L1MU8F \
                             HLT_TestChain8_ev1_L1EM3 \
                             HLT_TestChain6_mv1_TestChain10_ev1_L12eEM10L_MU8F\
                             HLT_TestChain6_mv2_TestChain8_ev2_L12eEM10L_MU8F'
    data['l1emroi'][3]   =  '-0.6,1.7,0,EM3,EM7;'
    data['emclusters'][3]=  'eta:-0.6,phi:1.7,et:9000;'
    data['l1muroi'][3]   =  '-1.7,-0.2,0,MU5VF,MU8F,MU8VF;'
    data['msmu'][3]      =  'eta:-1.7,phi:-0.2,pt:29500,pt2:8500;'

    # otehr vectors

    data['tracks'] = ['eta:1,phi:1,pt:120000; eta:1,phi:-1.2,et:32000;',
                      'eta:1,phi:1,pt:120000; eta:1,phi:-1.2,et:32000;',
                      'eta:0.5,phi:0,pt:130000; eta:1,phi:-1.2,pt:60000;eta:-1.2,phi:0.7,pt:6700; eta:-1.1,phi:0.6,pt:8600;',
                      'eta:-0.6,phi:1.7,et:9000;'] # no MU track for MS candidate 'eta:-1.7,phi:-0.2,pt:9500;'

    data['mucomb'] = [';',
                      ';',
                      'eta:-1.2,phi:0.7,pt:6600; eta:-1.1,phi:0.6,pt:8600;',
                      ';']

    data['electrons'] = [';',
                         'eta:1,phi:1,pt:120000; eta:1,phi:-1.2,et:32000;',
                         ';',
                         ';']
    data['photons'] = [';',
                       'eta:1,phi:1,pt:130000;',
                       ';',
                       ';']

    from TriggerMenuMT.CFtest.TestUtils import writeEmulationFiles
    writeEmulationFiles(data)


###########################################################################    
def generateChainsManually(flags, maskbit=0x7):
    """
    generates chains without menu, directly adding Chain configuration to HLTConfig
    maskbits used to enable signature-lke group of chains
    """
    log.info("generateChainsManually mask=0x%d",maskbit)
    from TriggerMenuMT.CFtest.TestUtils import makeChain, makeChainStep
    from TriggerMenuMT.HLT.Config.MenuComponents import EmptyMenuSequence
    doMuon     = maskbit & 0x1
    doElectron = maskbit>>1 & 0x1
    doCombo    = maskbit>>2 & 0x1

    HLTChains = []

    # muon chains
    if doMuon:
        from TriggerMenuMT.CFtest.HLTSignatureConfig import  muMenuSequence
        #step1
        mu11 = functools.partial(muMenuSequence, flags,step="1",reconame="v1", hyponame="v1")
        mu12 = functools.partial(muMenuSequence,flags,step="1",reconame="v2", hyponame="v2")
                    
        #step2
        mu21 = functools.partial(muMenuSequence,flags,step="2",reconame="v1", hyponame="v1")
        mu22 = functools.partial(muMenuSequence,flags,step="2",reconame="v2", hyponame="v2")
        #step3
        mu31 = functools.partial(muMenuSequence,flags,step="3",reconame="v1", hyponame="v1")
        mu32 = functools.partial(muMenuSequence,flags,step="3",reconame="v2", hyponame="v2")
        #step4
        mu41 = functools.partial(muMenuSequence,flags,step="4",reconame="v1", hyponame="v1")

        step_mu11  = makeChainStep("Step1_mu11", [mu11] )
        step_mu21  = makeChainStep("Step2_mu21", [mu21] )
        step_mu22  = makeChainStep("Step2_mu22", [mu22] )
        step_mu31  = makeChainStep("Step3_mu31", [mu31] )
        step_mu32  = makeChainStep("Step3_mu32", [mu32] )
        step_mu41  = makeChainStep("Step4_mu41", [mu41] )
        
        step_empy= makeChainStep("Step2_mu1empty", isEmpty=True)

        MuChains  = [
            makeChain(flags, name='HLT_TestChain8_mv1step_L1MU8F', L1Thresholds=["MU5VF"],    ChainSteps=[step_mu11]),
            makeChain(flags, name='HLT_TestChain8_mv1_L1MU8F',    L1Thresholds=["MU8F"],   ChainSteps=[step_mu11 , step_mu21 , step_mu31, step_mu41] ),
            makeChain(flags, name='HLT_TestChain20_mv1_L1MU8F',   L1Thresholds=["MU8F"],   ChainSteps=[step_mu11 , step_mu21 , step_mu31, step_mu41] ),
            makeChain(flags, name='HLT_TestChain10_mv2_L1MU8F',   L1Thresholds=["MU8F"],   ChainSteps=[step_mu11 , step_mu22 , step_mu31] ),
            makeChain(flags, name='HLT_TestChain8_mEmpty2_L1MU8F', L1Thresholds=["MU5VF"],    ChainSteps=[step_mu11 , step_empy , step_mu32, step_mu41] )
            ]
                

        HLTChains += MuChains


    ## #electron chains
    if doElectron:
        from TriggerMenuMT.CFtest.HLTSignatureConfig import  elMenuSequence, gamMenuSequence
        el11 = functools.partial(elMenuSequence,flags,step="1",reconame="v1", hyponame="v1")
        el21 = functools.partial(elMenuSequence,flags,step="2",reconame="v1", hyponame="v1")
        el22 = functools.partial(elMenuSequence,flags,step="2",reconame="v2", hyponame="v2")
        el23 = functools.partial(elMenuSequence,flags,step="2",reconame="v2", hyponame="v3")
        el31 = functools.partial(elMenuSequence,flags,step="3",reconame="v1", hyponame="v1")
        el41 = functools.partial(elMenuSequence,flags,step="4",reconame="v1", hyponame="v1")

        # gamma
        gamm11 = functools.partial(gamMenuSequence,flags,"1", reconame="v1", hyponame="v1")
    
        ElChains  = [
            makeChain(flags, name='HLT_TestChain5_ev1_L1EM3', L1Thresholds=["EM3"], ChainSteps=[ makeChainStep("Step1_em11", [el11]), makeChainStep("Step2_em21",  [el21]), makeChainStep("Step3_em31",  [el31])] ),
            makeChain(flags, name='HLT_TestChain8_ev1_L1EM3', L1Thresholds=["EM3"], ChainSteps=[ makeChainStep("Step1_em11", [el11]), makeChainStep("Step2_em21",  [el21]), makeChainStep("Step3_em31",  [el31]) ] ),
            makeChain(flags, name='HLT_TestChain5_ev2_L1EM7', L1Thresholds=["EM7"], ChainSteps=[ makeChainStep("Step1_em11", [el11]), makeChainStep("Step2_em22",  [el22]) ] ),
            makeChain(flags, name='HLT_TestChain5_ev3_L1EM7', L1Thresholds=["EM7"], ChainSteps=[ makeChainStep("Step1_em11", [el11]), makeChainStep("Step2_em23",  [el23]) ] ),
            makeChain(flags, name='HLT_TestChain5_gv1_L1EM7', L1Thresholds=["EM7"], ChainSteps=[ makeChainStep("Step1_gam11", [gamm11]) ] )
        ]        

        HLTChains += ElChains
        

    # combined chain
    if doCombo:

        emptySeq1 = functools.partial(EmptyMenuSequence,"step1EmptySeqence")
        emptySeq2 = functools.partial(EmptyMenuSequence,"step2EmptySeqence")
        
        if not doElectron:
            from TriggerMenuMT.CFtest.HLTSignatureConfig import elMenuSequence        
            el11 = functools.partial(elMenuSequence,flags,step="1",reconame="v1", hyponame="v1")
            el21 = functools.partial(elMenuSequence,flags,step="2",reconame="v1", hyponame="v1")
            el41 = functools.partial(elMenuSequence,flags,step="4",reconame="v1", hyponame="v1")
            
        if not doMuon:
            from TriggerMenuMT.CFtest.HLTSignatureConfig import muMenuSequence
            #step1
            mu11 = functools.partial(muMenuSequence,flags,step="1",reconame="v1", hyponame="v1")
            mu12 = functools.partial(muMenuSequence,flags,step="1",reconame="v2", hyponame="v2")
            #step2
            mu21 = functools.partial(muMenuSequence,flags,step="2",reconame="v1", hyponame="v1")
            mu22 = functools.partial(muMenuSequence,flags,step="2",reconame="v2", hyponame="v2")
            #step3
            mu31 = functools.partial(muMenuSequence,flags,step="3",reconame="v1", hyponame="v1")
            mu32 = functools.partial(muMenuSequence,flags,step="3",reconame="v2", hyponame="v2")
            #step4
            mu41 = functools.partial(muMenuSequence,flags,step="4",reconame="v1", hyponame="v1")
           
           
        from TriggerMenuMT.CFtest.HLTSignatureHypoTools import dimuDrComboHypoTool
                            

        CombChains =[
            # This is an example of a chain running in "serial"
            makeChain(flags, name='HLT_TestChain6_mv1_TestChain10_ev1_L12eEM10L_MU8F',  L1Thresholds=["MU5VF","EM3"], ChainSteps=[
                makeChainStep("Step1_mu_em_serial", [mu11, emptySeq1]),
                makeChainStep("Step2_mu_em_serial", [emptySeq2, el21]),
                makeChainStep("Step3_mu_em_serial",  isEmpty=True),
                makeChainStep("Step4_mu_em_serial", [mu41, el41])] ),

            makeChain(flags, name='HLT_TestChain6_mv2_TestChain8_ev2_L12eEM10L_MU8F', L1Thresholds=["MU5VF","EM3"], ChainSteps=[
                makeChainStep("Step1_mu2_em", [mu12, el11]),
                makeChainStep("Step2_mu_em", [mu21, el21])] ),

            makeChain(flags, name='HLT_TestChain5_ev1_TestChain8_ev1_L12EM3',   L1Thresholds=["EM3","EM3"], ChainSteps=[ #norun
                makeChainStep("Step1_2emAs",   [el11, el11]),
                makeChainStep("Step2_2emAs",   [el21, el21]) ]),
                
            makeChain(flags, name='HLT_TestChain5_ev1_TestChain8_ev1_2TestChain6_mv1_L12EM8VH_MU8F',   L1Thresholds=["EM8VH","EM8VH","MU8F"], ChainSteps=[
                makeChainStep("Step1_2em_2mu",   [el11,el11,mu11]),
                makeChainStep("Step2_2em_2mu",   [el21,el21,mu21]) ]),

            makeChain(flags, name='HLT_2TestChain6_mv1_L12MU5VF',       L1Thresholds=["MU5VF"], ChainSteps=[
                makeChainStep("Step1_2mu",   [mu11]),
                makeChainStep("Step2_2mu",   [mu21]) ]),

            makeChain(flags, name='HLT_3TestChain6_mv1_L12MU5VF',       L1Thresholds=["MU5VF"], ChainSteps=[
                makeChainStep("Step1_2mu",   [mu11]),
                makeChainStep("Step2_2mu",   [mu21]) ]),

            makeChain(flags, name='HLT_TestChain6_mv1_TestChain10_mv1_L12MU5VF',       L1Thresholds=["MU5VF", "MU5VF"], ChainSteps=[
                makeChainStep("Step1_2muAs",   [mu11,mu11]),
                makeChainStep("Step2_2muAs",   [mu21,mu21]) ]),
                
            makeChain(flags, name='HLT_2TestChain6_mEmpty1_L12MU5VF',   L1Thresholds=["MU5VF"], ChainSteps=[
                makeChainStep("Step1_2mu_empty", isEmpty=True),
                makeChainStep("Step2_2mu", [mu21]) ]),

            makeChain(flags, name='HLT_TestChain6_mv1_TestChain5_ev1dr_L12MU5VF',  L1Thresholds=["MU5VF","EM3"], ChainSteps=[
                makeChainStep("Step1_mu_em", [mu11, el11], comboToolConfs=[dimuDrComboHypoTool]),
                makeChainStep("Step2_mu_em", [mu21, el21], comboToolConfs=[dimuDrComboHypoTool])] ),
                                                                             
           
            makeChain(flags, name='HLT_2TestChain4_mv1dr_L12MU5VF', L1Thresholds=["MU5VF"], ChainSteps=[
                makeChainStep("Step1_2mu",    [mu11], comboToolConfs=[dimuDrComboHypoTool]),
                makeChainStep("Step2_2mu22",  [mu22]) ] ),

            # FSNOSEED not implemented in emulation
            #  L1Thresholds=["MU5VF", "MU5VF"],
            makeChain(flags, name='HLT_TestChain10_mEmpty1_TestChain6_mEmpty1_L12MU5VF', L1Thresholds=["MU5VF", "MU5VF"],  ChainSteps=[                 
                 makeChainStep("Step1_2muAs_empty", isEmpty=True),
                 makeChainStep("Step2_2muAs",   [mu21, mu21]) ])
            ]
           
        HLTChains += CombChains

    return HLTChains


########################## L1 #################################################        

def emulateHLTSeedingCfg(flags, seqName = None):
    """
    copy of HLTSeeding/python/HLTSeedingConfig.py to allow seeding with emulated data with CA
    """

    from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator    
    acc = ComponentAccumulator()
    
    decoderAlg = CompFactory.HLTSeeding()
    decoderAlg.RoIBResult = "RoIBResult"  # emulation based on legacy L1 data flow
    decoderAlg.L1TriggerResult = ""  # emulation based on legacy L1 data flow
    decoderAlg.HLTSeedingSummaryKey = "HLTSeedingSummary" # Transient, consumed by DecisionSummaryMakerAlg
    ## emulate CTP:
    decoderAlg.ctpUnpacker = CompFactory.CTPUnpackingEmulationTool( ForceEnableAllChains=False , InputFilename="ctp.dat" )

    from TrigEDMConfig.TriggerEDM import recordable
    from HLTSeeding.HLTSeedingConfig import mapThresholdToL1RoICollection, mapThresholdToL1DecisionCollection, createKeyWriterTool
    decoderAlg.RoIBRoIUnpackers += [
        CompFactory.FSRoIsUnpackingTool("FSRoIsUnpackingTool", Decisions=mapThresholdToL1DecisionCollection("FSNOSEED"),
                                        OutputTrigRoIs = recordable(mapThresholdToL1RoICollection("FSNOSEED")) ) ]

    # emulate prescaler:

    decoderAlg.prescaler = CompFactory.PrescalingEmulationTool()
    decoderAlg.KeyWriterTool = createKeyWriterTool()
    decoderAlg.DoCostMonitoring = False

    # emulate L1 Unpackers
    emUnpacker = CompFactory.RoIsUnpackingEmulationTool("EMRoIsEmuUnpackingTool", InputFilename="l1emroi.dat", OutputTrigRoIs=mapThresholdToL1RoICollection("EM"), Decisions=mapThresholdToL1DecisionCollection("EM"), ThresholdPrefix="EM" )
    muUnpacker = CompFactory.RoIsUnpackingEmulationTool("MURoIsEmuUnpackingTool", InputFilename="l1muroi.dat",  OutputTrigRoIs=mapThresholdToL1RoICollection("MU"), Decisions=mapThresholdToL1DecisionCollection("MU"), ThresholdPrefix="MU" )
    decoderAlg.RoIBRoIUnpackers = [emUnpacker, muUnpacker]

    acc.addEventAlgo( decoderAlg, sequenceName = seqName )

    from TrigConfigSvc.TrigConfigSvcCfg import TrigConfigSvcCfg, HLTPrescaleCondAlgCfg
    acc.merge( TrigConfigSvcCfg( flags ) )
    acc.merge( HLTPrescaleCondAlgCfg( flags ) )

    return acc
